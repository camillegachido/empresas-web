import styled, { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
   * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
  }

  body{
    background-color: #ebe9d7;
    font-size: 16px;
  }

  .text-decoration-none{
    text-decoration: none;
  }

  @media only screen and (max-width: 600px) {
    body{
      font-size: 12px;
    }
  }
`;

export const Container = styled.main`
  padding: 3.185em;
`;

export const Header = styled.header`
  width: 100%;
  padding: 2.81em 2.5em;
  position: relative;
  background-color: #ee4c77;

  img {
    display: block;
    margin: auto;
    width: 14em;
  }
`;
