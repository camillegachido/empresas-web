import axios from "axios";

export const url = "http://localhost:3000";
export const initial_url = "https://empresas.ioasys.com.br";
export const api_url = initial_url + "/api/v1/";
export const api = axios.create({ baseURL: api_url });

api.interceptors.response.use(
  (response) => response,
  (error) => {
    if (401 === error.response.status) {
    }
    return error.response;
  }
);
