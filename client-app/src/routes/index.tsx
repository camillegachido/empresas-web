import { useEffect, useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { Login, Home, Company } from "../pages";

function Router() {
  const navigate = useNavigate();
  const [user, setUser] = useState(false);

  useEffect(() => {
    if (user) {
      navigate("/enterprise");
    }
  }, [user]);

  return (
    <Routes>
      <Route path="/" element={<Login setUser={setUser} />} />
      {user ? (
        <>
          <Route path="/enterprise">
            <Route index element={<Home />} />
            <Route path=":id" element={<Company />} />
          </Route>
        </>
      ) : (
        <></>
      )}
    </Routes>
  );
}

export default Router;
