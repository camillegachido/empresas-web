import styled from "styled-components";

export const Container = styled.div`
  width: 80%;
  max-width: 25.938em;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Logo = styled.img`
  display: block;
  margin: 4.188em auto;
`;

export const Title = styled.h1`
  font-size: 1.5em;
  font-weight: bold;
  text-align: center;
  color: #383743;
`;

export const Paragraph = styled.p`
  margin-top: 1.281em;
  font-size: 1.125em;
  line-height: 1.44;
  letter-spacing: -0.25px;
  text-align: center;
  color: #383743;
`;

interface ErrorProps {
  error: boolean;
}

export const InputContent = styled.div<ErrorProps>`
  position: relative;

  &:before {
    position: absolute;
    background-color: #ff0f44;
    height: calc(1.5em - 4px);
    width: 1.5em;
    content: "!";
    border-radius: 50%;
    top: 39px;
    right: 5px;
    color: #fff;
    text-align: center;
    padding-top: 4px;
    display: ${(props) => (props.error ? "block" : "none")};
  }
`;

interface InputProps {
  imageUrl: string;
}

export const Input = styled.input<InputProps>`
  width: 100%;
  border: 0;
  background-color: transparent;
  padding: 1em 2.278em;
  border-bottom: solid 0.7px #383743;
  background-image: url(${(props) => props.imageUrl});
  background-repeat: no-repeat;
  background-position: left;
  margin: 1.4em 0;
  font-size: 1.125em;
`;

export const Button = styled.button`
  width: calc(100% - 7.919em);
  border: none;
  border-radius: 3.6px;
  padding: 0.9em 0;
  color: #fff;
  text-align: center;
  background-color: #57bbbc;
  display: block;
  margin: auto;
`;

export const ErrorMessage = styled.p<ErrorProps>`
  color: ${(props) => (props.error ? "#ff0f44" : "transparent")};
  text-align: center;
  margin: 1em 0;
`;
