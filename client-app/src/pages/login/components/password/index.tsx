import React from "react";
import * as S from "../../styles";
import { Container } from "./styles";

import Locker from "../../assets/ic-cadeado.png";

interface IProps {
  value: string;
  setPassword: React.Dispatch<React.SetStateAction<string>>;
}

export function Password({ value, setPassword }: IProps) {
  return (
    <Container>
      <S.Input
        type="password"
        placeholder="Senha"
        imageUrl={Locker}
        value={value}
        onChange={({ target }) => setPassword(target.value)}
      ></S.Input>
    </Container>
  );
}
