import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { api } from "../../services";
import { Password } from "./components";

import * as S from "./styles";

import Logo from "./assets/logo-home.png";
import Logo2x from "./assets/logo-home@2x.png";
import Logo3x from "./assets/logo-home@3x.png";
import Email from "./assets/ic-email.png";

interface ILogin {
  success: boolean;
  errors: string[];
}

interface IProps {
  setUser: React.Dispatch<React.SetStateAction<boolean>>;
}

export function Login({ setUser }: IProps) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(
    "Credenciais informadas são inválidas, tente novamente."
  );

  const signIn = async () => {
    let getUserFormData = new FormData();
    getUserFormData.append("email", email);
    getUserFormData.append("password", password);

    const response = await api.post<ILogin>(
      `users/auth/sign_in`,
      getUserFormData
    );

    if (response.data.success) {
      api.defaults.headers.common["access-token"] =
        response.headers["access-token"];
      api.defaults.headers.common["client"] = response.headers["client"];
      api.defaults.headers.common["uid"] = response.headers["uid"];
      setError(false);
      setUser(true);
    } else {
      setUser(false);
      setError(true);
      setMessage(response.data.errors[0]);
    }
  };

  return (
    <S.Container>
      <S.Logo src={Logo} srcSet={`${Logo2x} 2x, ${Logo3x} 3x`} />
      <S.Title>BEM-VINDO AO EMPRESAS</S.Title>
      <S.Paragraph>
        {" "}
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </S.Paragraph>
      <S.InputContent error={error}>
        <S.Input
          type="email"
          placeholder="Email"
          imageUrl={Email}
          value={email}
          onChange={({ target }) => setEmail(target.value)}
        />
      </S.InputContent>
      <S.InputContent error={error}>
        <Password value={password} setPassword={setPassword} />
      </S.InputContent>
      <S.ErrorMessage error={error}>{message}</S.ErrorMessage>
      <S.Button onClick={signIn}>ENTRAR</S.Button>
    </S.Container>
  );
}
