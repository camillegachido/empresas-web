import styled from "styled-components";

export const Message = styled.p`
  width: 100%;
  font-size: 2em;
  line-height: 1.22;
  letter-spacing: -0.45px;
  text-align: center;
  color: #383743;

  margin-top: calc(50vh - 150px);
`;
