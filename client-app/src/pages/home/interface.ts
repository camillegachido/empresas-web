import { ICompany } from "../../common/interface";

export interface ICompanyResponse {
  enterprises: ICompany[];
}
