import React, { useState } from "react";

import * as S from "./styles";
import * as G from "../../../../common/styles";

import { api } from "../../../../services";

import Logo from "../../assets/logo-nav.png";
import Logo2x from "../../assets/logo-nav@2x.png";
import Logo3x from "../../assets/logo-nav@3x.png";
import { ICompanyResponse } from "../../interface";
import { ICompany } from "../../../../common/interface";

interface IProps {
  setCompanies: React.Dispatch<React.SetStateAction<ICompany[]>>;
}

export function Header({ setCompanies }: IProps) {
  const [search, setSearch] = useState("");
  const [show, setShow] = useState(false);

  const handleOnSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const params = { name: search };
    const response = await api.get<ICompanyResponse>("enterprises", { params });
    setCompanies(response.data.enterprises);
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value.toLowerCase());
  };

  return (
    <G.Header>
      <img
        src={Logo}
        srcSet={`${Logo2x} 2x, ${Logo3x} 3x`}
        alt="Logo iaosys branco"
      />
      <form onSubmit={handleOnSubmit}>
        <S.ContainerInput show={show}>
          <input type="text" onChange={handleSearchChange} />
          <button id="close" onClick={() => setShow(false)} hidden={!show}>
            x
          </button>
          <button
            onClick={() => setShow(true)}
            hidden={show}
            id="search"
          ></button>
        </S.ContainerInput>
      </form>
    </G.Header>
  );
}
