import styled from "styled-components";

import Search from "../../assets/ic-search.png";

interface ISearch {
  show: boolean;
}

export const ContainerInput = styled.div<ISearch>`
  position: absolute;
  right: 2.5em;
  top: 2.81em;
  width: calc(100% - 5em);

  input {
    outline: none;
    right: 0px;
    position: absolute;
    background-image: url(${Search});
    background-color: #ee4c77;
    padding: 1em 2.278em 1em 60px;
    border: 0;
    border-bottom: ${(props) => (props.show ? "solid 0.7px #fff" : "0px")};
    background-repeat: no-repeat;
    background-position: left;
    font-size: 1.125em;
    z-index: 1;
    color: #fff;
    width: ${(props) => (props.show ? "100%" : "0px")};
    transition: all 1s;
  }

  button:hover {
    cursor: pointer;
  }

  #close {
    position: absolute;
    z-index: 2;
    color: #fff;
    font-size: 2em;
    right: 1em;
    top: 0.5em;

    border: 0;
    background-color: transparent;
  }

  #search {
    position: absolute;
    z-index: 2;
    color: #fff;
    font-size: 2em;
    right: 34px;
    top: 0;
    height: 60px;
    width: 60px;

    border: 0;
    background-color: transparent;
  }
`;
