import React from "react";

import * as S from "./styles";
import { initial_url } from "../../../../services";
import { ICompany } from "../../../../common/interface";
import { Link } from "react-router-dom";

interface IProps {
  company: ICompany;
}

export function Company({ company }: IProps) {
  const { photo, enterprise_name, enterprise_type, country, id } = company;

  return (
    <Link to={`/enterprise/${id}`} className="text-decoration-none">
      <S.Company>
        <img src={`${initial_url + photo}`} alt={enterprise_name} />
        <div>
          <S.Name>{enterprise_name}</S.Name>
          <S.Type>{enterprise_type.enterprise_type_name}</S.Type>
          <S.Country>{country}</S.Country>
        </div>
      </S.Company>
    </Link>
  );
}
