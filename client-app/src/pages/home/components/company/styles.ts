import styled from "styled-components";

export const Company = styled.article`
  width: 100%;
  display: flex;
  padding: 1.688em;
  justify-content: space-between;
  background-color: #fff;
  margin-bottom: 2.185em;

  img {
    width: 18.313em;
    height: 10em;
    object-fit: cover;
  }

  div {
    margin-left: 18px;
    width: 100%;
  }

  @media only screen and (max-width: 550px) {
    display: block;

    img {
      width: 100%;
    }

    div {
      margin-left: 0px;
      margin-top: 1em;
    }
  }
`;

export const Name = styled.h1`
  font-size: 1.875em;
  font-weight: bold;
  color: #1a0e49;
  margin: 0;
`;

export const Type = styled.h2`
  font-size: 1.5em;
  color: #8d8c8c;
`;

export const Country = styled.p`
  font-size: 1.125em;
  color: #8d8c8c;
`;
