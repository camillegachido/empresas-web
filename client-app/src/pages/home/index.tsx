import React, { useState } from "react";

import { Header, Company } from "./components";
import { ICompany } from "../../common/interface";

import * as S from "./styles";
import * as G from "../../common/styles";

export function Home() {
  const [companies, setCompanies] = useState<ICompany[]>([]);

  return (
    <>
      <Header setCompanies={setCompanies} />
      <G.Container>
        {companies.length > 0 ? (
          companies.map((company) => (
            <Company company={company} key={company.id} />
          ))
        ) : (
          <S.Message>Clique na busca para iniciar.</S.Message>
        )}
      </G.Container>
    </>
  );
}
