import { ICompany } from "../../common/interface";

export interface LocationState {
  id: string;
}

export interface ICompanyResponse {
  enterprise: ICompany;
  success: boolean;
}
