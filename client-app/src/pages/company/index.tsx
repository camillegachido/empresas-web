import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ICompanyResponse } from "./interface";

import * as S from "./styles";
import * as G from "../../common/styles";

import { ICompany } from "../../common/interface";

import { api, initial_url } from "../../services";
import { Header } from "./components";

export function Company() {
  let { id } = useParams<"id">();

  const [company, setCompany] = useState<ICompany>({} as ICompany);

  const getCompany = async () => {
    const response = await api.get<ICompanyResponse>(`enterprises/${id}`);
    setCompany(response.data.enterprise);
  };

  useEffect(() => {
    getCompany();
  }, [id]);

  return (
    <>
      <Header name={company.enterprise_name} />
      <G.Container>
        <S.Content>
          <img
            src={`${initial_url + company.photo}`}
            alt={company.enterprise_name}
          />
          <p>{company.description}</p>
        </S.Content>
      </G.Container>
    </>
  );
}
