import React from "react";

import * as S from "./styles";
import * as G from "../../../../common/styles";
import { Link } from "react-router-dom";

interface IProps {
  name: string;
}

export function Header({ name }: IProps) {
  return (
    <G.Header>
      <S.Container>
        <Link to="/enterprise">
          <S.Back />
        </Link>
        <S.Title>{name}</S.Title>
      </S.Container>
    </G.Header>
  );
}
