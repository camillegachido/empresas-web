import styled from "styled-components";
import Arrow from "../../assets/ic-flecha.png";

export const Container = styled.div`
  display: flex;
`;

export const Title = styled.h1`
  color: #fff;
  font-size: 2.125em;
  font-weight: normal;
  margin-left: 36px;
`;

export const Back = styled.button`
  height: 2.125em;
  width: 2.125em;
  border: 0;
  background-color: transparent;
  background-image: url(${Arrow});
  background-position: center;
  background-size: 2.125em 2.125em;
  background-repeat: no-repeat;

  &:hover {
    cursor: pointer;
  }
`;
