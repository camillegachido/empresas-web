import styled from "styled-components";

export const Content = styled.section`
  background-color: #fff;
  margin: auto;
  padding: 3em 4.5em;
  min-height: 70vh;

  img {
    width: 100%;
    height: 18.438em;
    object-fit: cover;
  }

  p {
    font-size: 1.125em;
    font-weight: normal;
    color: #8d8c8c;
    margin-top: 1em;
  }
`;
