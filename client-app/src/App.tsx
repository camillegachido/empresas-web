import React from "react";
import { BrowserRouter } from "react-router-dom";
import Router from "./routes";

import GlobalStyles from "./common/styles";

export default function Login() {
  return (
    <>
      <BrowserRouter>
        <Router />
      </BrowserRouter>

      <GlobalStyles />
    </>
  );
}
